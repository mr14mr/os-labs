//
// Created by Тимур Мустафин on 10/3/18.
//

#include <stdio.h>

int main(int argc, char **argv) {
    int count = 0;
    printf("How many Ns you want to see?:\n");
    scanf("%d", &count);

    int *array = malloc(count * sizeof(int));

    for (int i = 0; i < count; ++i) {
        array[i] = i;
        printf("El %d added\n", i);
    }

    free(array);

}