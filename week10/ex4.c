//
// Created by Тимур Мустафин on 10/25/18.
//

// DECLAIMER: I didn't see the st_nlink attr. Yeah, it's one more ВЕЛОСИПЕД (For non-russian speaking people:
// implementation of well-known thing). But there is no time left. I hope you won't punish me.


// DECLAIMER 2: You're reading the solution which I completed after deadline (12:01 am). I understood that my program
// working incorrect and fixed it. Didn't fix my ВЕЛОСИПЕД just for let you make a little bit smiling of my stupidity.

#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>

#define MAX_ALIASES_COUNT 100
#define MAX_FILENAME_LENGTH 100
#define DIRECTORY_TO_SCAN "tmp"
#define MAX_FILES_COUNT 10000


struct my_inode {
    int id;
    int counter;
    int file_count;
    char files[MAX_ALIASES_COUNT][MAX_FILENAME_LENGTH];
};

struct my_inode my_inodes[MAX_FILES_COUNT];
int my_inodes_count = 0;

void add_to_inodes(int inode_number, char *filename) {
    for (int i = 0; i < my_inodes_count; ++i) {
        if (my_inodes[i].id == inode_number) {
            my_inodes[i].counter++;
            my_inodes[i].file_count++;
            strcpy(my_inodes[i].files[my_inodes[i].file_count - 1], filename);
            return;
        }
    }

    struct my_inode new_inode;
    new_inode.id = inode_number;
    new_inode.counter = 1;
    new_inode.file_count = 1;
    strcpy(new_inode.files[new_inode.file_count - 1], filename);
    my_inodes[my_inodes_count] = new_inode;
    my_inodes_count++;
}

int main() {

    struct dirent *current_file;
    DIR *scnning_directory;

    struct stat *buf;

    scnning_directory = opendir(DIRECTORY_TO_SCAN);

    while ((current_file = readdir(scnning_directory)) != NULL) {
        buf = malloc(sizeof(struct stat));
        char file_name[MAX_FILENAME_LENGTH] = DIRECTORY_TO_SCAN;
        strcat(file_name, "/");
        strcat(file_name, current_file->d_name);
        stat(file_name, buf);

        add_to_inodes(buf->st_ino, current_file->d_name);

        free(buf);
    }

    for (int i = 0; i < my_inodes_count; ++i) {
        if (my_inodes[i].file_count < 2)
            continue;

        printf("inode %d has met %d times with this names\n", my_inodes[i].id, my_inodes[i].file_count);
        for (int j = 0; j < my_inodes[i].file_count; ++j) {
            printf("   %s\n", my_inodes[i].files[j]);
        }
    }

}