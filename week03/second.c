//
// Created by Тимур Мустафин on 9/5/18.
//

#include <stdio.h>

void swap(int *a, int *b) {
    int tmp = *b;
    *b = *a;
    *a = tmp;
}

void bubble_sort(int *arr, int n) {
    for (int i = 0; i < n; ++i) {
        for (int j = 1; j < n - i; ++j) {
            if (arr[j - 1] > arr[j])
                swap(&arr[j - 1], &arr[j]);
        }
    }
}

int main() {
    int a[5] = {0, 1, 5, 4, 3};
    bubble_sort(a, 5);
    for (int i = 0; i < 5; ++i) {
        printf("%d ", a[i]);
    }
}