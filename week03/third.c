//
// Created by Тимур Мустафин on 9/5/18.
//

#include <stdlib.h>
#include <stdio.h>

typedef struct Node {
    int value;
    struct Node *next;
} Node;

typedef struct LinkedList {
    Node *head;
    int length;
} LinkedList;

/*
 * first argument — linked list, second — the value to insert, third — position for insert
 * */
void new_node(LinkedList *list, int value, int index) {

    if (index < 0 || index > list->length) {
        printf("Index out of range\n");
        return;
    }

    // in case it's the first element let's just save the value to the head of this list
    if (list->length == 0) {
        list->head->value = value;

        // increment the size
        list->length++;

        return;
    }

    Node *prev_node = list->head;

    for (int i = 0; i < index - 1; ++i) {
        prev_node = prev_node->next;
    }

    // create new node for our list
    Node *node_pointer = malloc(sizeof(Node));
    node_pointer->value = value;
    node_pointer->next = prev_node->next;

    prev_node->next = node_pointer;


    // increment the size
    list->length++;

}

/*
 * first argument — linked list, second — the value to delete
 * */
void delete_node(LinkedList *list, int value) {
    Node *current_node = list->head;
    Node *last_node = NULL;

    // check if the target is head
    if (list->head->value == value) {

        //if length is 1 then let the node and erase the value
        if (list->length == 1) {
            list->head->value = 0;
        } else {

            // else replace the head with another head
            Node *new_head = list->head->next;
            Node *old_head = list->head;

            list->head = new_head;
            free(old_head);
        }
        list->length--;

        return;

    }

    for (int i = 0; i < list->length; ++i) {

        // in case it's target element
        if (current_node->value == value) {
            last_node->next = current_node->next;
            list->length--;
            free(current_node);
            return;
        }

        // otherwise just go to the next
        last_node = current_node;
        current_node = current_node->next;
    }
}

void init_linked_list(LinkedList *list) {
    list->length = 0;
    Node *node_pointer = malloc(sizeof(Node));

    node_pointer->value = 0; // head is always contains 0 by default but user never access it.
    node_pointer->next = NULL;
    list->head = node_pointer;
}

void print_list(LinkedList *list) {
    Node *last_node = list->head;

    for (int i = 0; i < list->length; ++i) {
        printf("%d ", last_node->value);
        last_node = last_node->next;
    }

}

int main() {
    LinkedList linkedList;
    init_linked_list(&linkedList);

    new_node(&linkedList, 10, 0);
    new_node(&linkedList, 20, 1);
    new_node(&linkedList, 30, 2);
    new_node(&linkedList, 40, 3);
    new_node(&linkedList, 50, 4);
    new_node(&linkedList, 45, 4);
    printf("init\n");

    delete_node(&linkedList, 10);
    printf("after delete\n");
    print_list(&linkedList);
}

