//
// Created by Тимур Мустафин on 10/11/18.
//

#include <stdio.h>
#include <sys/resource.h>

#define MY_10M (1024 * 10 * 8)

void main() {
    for (int i = 0; i < 10; ++i) {
        struct rusage u;
        int *ptr = malloc(MY_10M);
        memset(ptr, 0, MY_10M);
        getrusage(RUSAGE_SELF, &u);
        printf(" %ld \n", u.ru_maxrss);
        sleep(1);
    }
}