//
// Created by Тимур Мустафин on 9/12/18.
//

#include <stdio.h>

char *trim(char *line) {
    int j;
    for (int i = 0; line[i] != '\0'; ++i) {
        while (!((line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z') || line[i] == '\0')) {
            for (j = i; line[j] != '\0'; ++j) {
                line[j] = line[j + 1];
            }
            line[j] = '\0';
        }
    }
    return line;
}

int main() {

    char input_command[100];

    while (1) {

        printf("os-lab$ ");
        fgets(input_command, 100, stdin);

        trim(input_command);

        // if we want to close out shell
        if (strcmp(input_command, "exit") == 0)
            return 0;
        else if (strlen(input_command) != 0)
            system(input_command);
        else
            printf("Not valid command\n");
    }
}
