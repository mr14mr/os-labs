//
// Created by Тимур Мустафин on 9/12/18.
//

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
//#include <unistd.h>
//#include <sys/wait.h>
//#include <sys/types.h>
//#include <string.h>

char *trim(char *line) {
    int j;
    for (int i = 0; line[i] != '\0'; ++i) {
        while (!((line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z') || line[i] == '\0' ||
                 line[i] == ' ' || line[i] == '-' || line[i] == '.')) {
            for (j = i; line[j] != '\0'; ++j) {
                line[j] = line[j + 1];
            }
            line[j] = '\0';
        }
    }
    return line;
}

int main() {

    char input_command[100];
    while (1) {

        printf("os-lab$ ");
        fgets(input_command, 100, stdin);

        trim(input_command);

        // if we want to close out shell
        if (strcmp(input_command, "exit") == 0)
            return 0;
        else if (strlen(input_command) != 0) {

            int pid = fork();
            if (pid == 0) { // if it's a child just do what we asked to do
                char *argv[] = {"/bin/sh", "-c", input_command, 0};
                execve(argv[0], &argv[0], NULL);
                return 0;
            }


        } else
            printf("Not valid command\n");
    }
}