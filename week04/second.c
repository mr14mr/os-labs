//
// Created by Тимур Мустафин on 9/12/18.
//

#include <stdio.h>
#include <zconf.h>

int main() {
    for (int i = 0; i < 3; i++) {
        int pid = fork();
        printf("pid is %d\n", pid);
        sleep(5);
    }
    return 0;
}