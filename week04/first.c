//
// Created by Тимур Мустафин on 9/12/18.
//
#include <stdio.h>
#include <zconf.h>

int main() {
    int process_id = fork();
    int n = 10;
    if (process_id > 0)
        printf("Hello from parent [%d - %d]\n", process_id, n);
    else if (!process_id)
        printf("Hello from child [%d - %d]\n", process_id, n);
    return 0;
}