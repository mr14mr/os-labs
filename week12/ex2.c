//
// Created by Тимур Мустафин on 11/7/18.
//

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#define BUFFER_SIZE 1
#define MAX_FILENAME_SIZE 150

int main(int argc, char *argv[]) {
    char filename[MAX_FILENAME_SIZE] = "ex2.txt";

    if (argc == 3 && !strcmp(argv[1], "-a"))
        strcpy(filename, argv[2]);

    int fd = open(filename, O_WRONLY | O_CREAT); // | O_APPEND
    char buffer[BUFFER_SIZE], c;
    while (read(STDIN_FILENO, &buffer, BUFFER_SIZE)) {
        int status = write(fd, buffer, BUFFER_SIZE);
    }

}