For increasing Hit/Miss ratio we should increase count of nonunique values in our input data.
As count of unique values tends to size of table our fraction becomes less. Otherwise it will increase.

If N = size of table, then the best situation will be like(using python functions): range(0, N-1).
The worse situation will be in any case like range(0, N*k), where k >> N