//
// Created by Тимур Мустафин on 10/18/18.
//

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define MAX_INT_WITH_ONE (INT_MAX - (INT_MAX >> 1)) // this number looks like 100...00 and has length as MAX_INT

struct node {
    int weight;
    bool reference_bit;
    int value;
};

int get_page_from_storage(struct node *nodes, int value_to_find, int max_size) {
    for (int j = 0; j < max_size; ++j) {
        if (nodes[j].value == value_to_find)
            return j; //have found
    }
    return -1; //not in storage
}

void add_page_to_storage(struct node *nodes, int value, int index_to_insert) {
    struct node node;
    node.value = value;
    node.reference_bit = true; // when we add any value we always mark it as used
    node.weight = 0; // but weight initially 0

    nodes[index_to_insert] = node;
}

int get_last_used_index(struct node *nodes, int max_size) {
    int min_value_index = 0;
    for (int i = 0; i < max_size; i++) {
        if (nodes[i].weight < nodes[min_value_index].weight)
            min_value_index = i;
    }

    return min_value_index;
}

void shift_page_weight(struct node *nodes, int max_size) {

    for (int j = 0; j < max_size; j++) {
        nodes[j].weight = nodes[j].weight >> 1;

        if (nodes[j].reference_bit)
            nodes[j].weight = nodes[j].weight >> 1 | MAX_INT_WITH_ONE;
        nodes[j].reference_bit = false;

    }
}

int main(int argc, char *argv[]) {
    FILE *fp = fopen("input_ok.txt", "r");

    int current_size = 0;
    int value_to_find = 0;
    unsigned int max_size = atoi(argv[1]);
    unsigned int total_count = 0;
    unsigned int hit = 0;
    unsigned int miss = 0;

    struct node *nodes = malloc(sizeof(struct node) * max_size);

    while (fscanf(fp, " %d", &value_to_find) != EOF) {
        total_count++;
        int my_index = get_page_from_storage(nodes, value_to_find, max_size);
        if (my_index != -1) {
            hit++;
            nodes[my_index].reference_bit = true;
        } else {
            miss++;
            if (current_size != max_size) {
                add_page_to_storage(nodes, value_to_find, current_size);
                current_size++;
            } else {
                add_page_to_storage(nodes, value_to_find, get_last_used_index(nodes, max_size));
            }
        }
        shift_page_weight(nodes, max_size);
    }


    double rate = (double) miss / (double) hit;

    printf("Hit/Miss: %f\n", rate);

}