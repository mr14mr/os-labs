//
// Created by Тимур Мустафин on 9/19/18.
//

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define THREADS 5

void *perform_work(void *argument) {
    int passed_in_value;

    passed_in_value = (int) argument;
    printf("Thread number %d.\n", passed_in_value);
    pthread_exit(NULL);

}

int main(int argc, char **argv) {

    for (short index = 0; index < THREADS; ++index) {
        pthread_t thread;

        printf("Creating thread %d\n", index);
        pthread_create(&thread, NULL, perform_work, (void *) index);
        int result_code = pthread_join(thread, NULL);
        printf("Thread %d completed with code %d\n\n", index, result_code);
    }

    exit(0);
}