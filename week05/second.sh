#!/usr/bin/env bash

for i in {1..5000}
do

    while true;
    do
        if `ln "second.txt" "second.txt.lock"`;  then
            last_number=$(tail -n 1 "second.txt")
            echo "$(($last_number + 1))" >> "second.txt"
            rm "second.txt.lock"
            break
        fi
    done

done