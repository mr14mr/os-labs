//
// Created by Тимур Мустафин on 9/19/18.
//

# include <stdio.h>
# include <pthread.h>

# define BUFFER_SIZE 10

int buffer_index = 0;
char *buffer;

void *producer() {
    while (1) {
        if (buffer_index == BUFFER_SIZE) continue; // like sleep/unsleep but simpler.
        buffer[buffer_index++] = '1';
        printf("produce: %d \n", buffer_index);
    }

}

void *consumer() {
    while (1) {
        if (buffer_index == 0) continue; // like sleep/unsleep but simpler.

        buffer_index--;

        printf("consumer: %d \n", buffer_index);
    }
}

int main() {
    pthread_t producer_thread;
    pthread_t consumer_thread;

    buffer = (char *) malloc(sizeof(char) * BUFFER_SIZE);

    pthread_create(&producer_thread, NULL, producer, NULL);
    pthread_create(&consumer_thread, NULL, consumer, NULL);

    pthread_join(producer_thread, NULL);
    pthread_join(consumer_thread, NULL);


    return 0;
}



