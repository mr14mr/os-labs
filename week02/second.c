//
// Created by Тимур Мустафин on 8/29/18.
//

#include <stdio.h>
#include <memory.h>

#define MAX_LENGTH 100

int main() {

    char str[MAX_LENGTH];

    printf("Enter your string:");
    scanf("%s", str);

    long str_size = strlen(str);
    char new_str[str_size];

    int j = 0;

    for (j = 0; j < str_size; ++j) {
        new_str[str_size - j - 1] = str[j];
    }

    new_str[j] = '\0';


    printf("Your reversed string: %s", new_str);

    return 0;
}
