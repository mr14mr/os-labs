//
// Created by Тимур Мустафин on 8/29/18.
//

#include <stdio.h>
#include <memory.h>

#define MAX_LENGTH 100

void swap(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

int main() {
    char int1[MAX_LENGTH], int2[MAX_LENGTH];
    int first, second;

    printf("Enter your first int:");
    scanf("%s", int1);
    sscanf(int1, "%d", &first);

    printf("Enter your second int:");
    scanf("%s", int2);
    sscanf(int2, "%d", &second);

    swap(&first, &second);

    return 0;
}