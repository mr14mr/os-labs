//
// Created by Тимур Мустафин on 8/29/18.
//

#include <stdio.h>

int main(int argc, char *argv[]) {

    if (argc != 2) {
        printf("Wrong args. Should bne only one integer stands for the height of pyramid.");
        return 1;
    }

    int height;

    sscanf(argv[1], "%d", &height);

    int width = 2 * height - 1;
    int half = width / 2 + 1;


    for (int y = 0; y < height; y++) {
        for (int x = 0; x <= width; x++) {
            if (x >= half - y && x <= half + y) {
                printf("*");
            } else {
                printf(" ");
            }
        }

        printf("\n");
    }

    return 0;

}