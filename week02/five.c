//
// Created by Тимур Мустафин on 8/29/18.
//


#include <stdio.h>

void pyramid(int height) {

    int width = 2 * height - 1;
    int half = width / 2 + 1;


    for (int y = 0; y < height; y++) {
        for (int x = 0; x <= width; x++) {
            if (x >= half - y && x <= half + y) {
                printf("*");
            } else {
                printf(" ");
            }
        }

        printf("\n");
    }
}

void square(int height) {

    int width = height;

    for (int y = 0; y < height; y++) {
        for (int x = 0; x <= width; x++) {
            if (x == 0 || x == width || y == 0 || y == height - 1) {
                printf("* ");
            } else {
                printf("  ");
            }
        }

        printf("\n");
    }
}

void arrow(int height) {

    int width = 2 * height - 1;
    int half = width / 2 + 1;

    for (int y = height; y >= height / 2 + 1; y--) {
        for (int x = 0; x <= width; x++) {
            if (x > half - y) {
                printf(" ");
            } else {
                printf("*");
            }
        }

        printf("\n");
    }


    for (int y = height / 2; y <= height; y++) {
        for (int x = 0; x <= width; x++) {
            if (y <= half - x) {
                printf("*");
            } else {
                printf(" ");
            }
        }

        printf("\n");
    }
}

int main(int argc, char *argv[]) {

    printf("Choose your figure: 0 - pyramid, 1 - square, 2 - arrow\n");

    if (argc != 3) {
        printf("Wrong args. Should two: first integer stands for the figure type, second is a coefficient "
               "which is related for the size of the figure.\n");
        return 1;
    }
    int type, size;
    sscanf(argv[1], "%d", &type);
    sscanf(argv[2], "%d", &size);

    printf("%d %d \n", type, size);


    switch (type) {
        case 0: {
            pyramid(size);
            break;
        }
        case 1: {
            square(size);
            break;
        }
        case 2: {
            arrow(size);
            break;
        }
    }

    return 0;

}