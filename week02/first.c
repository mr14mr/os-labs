//
// Created by Тимур Мустафин on 8/29/18.
//

#include <stdio.h>
#include <limits.h>
#include <float.h>

int main() {
    int max_int = INT_MAX;
    float float_max = FLT_MAX;
    double double_max = DBL_MAX;

    // max values
    printf("Max value for integer %d\n", max_int);
    printf("Max value for float %f\n", float_max);
    printf("Max value for double %e\n", double_max);

    printf("\n");

    // sizes for each type
    printf("Size for integer %d\n", sizeof(max_int));
    printf("Size for float %d\n", sizeof(float_max));
    printf("Size for double %d\n", sizeof(double_max));

    return 0;
}