//
// Created by Тимур Мустафин on 9/26/18.
//

#include <stdio.h>
#include <signal.h>

int main(int argc, char **argv) {
    int process_id = fork();

    if (process_id > 0) {
        sleep(10);
        kill(process_id, SIGKILL);
        printf("Ya tebya porodil, ya tebya i ubil.\n");
    } else if (!process_id) {
        while(1){
            printf("i'm alive\n");
            sleep(1);
        }
    }

}