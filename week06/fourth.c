//
// Created by Тимур Мустафин on 9/26/18.
//

#include <stdio.h>
#include <signal.h>

void signal_handler(int signal_int) {
    if (signal_int == SIGUSR1) {
        printf("SIGUSR1\n");
    } else if (signal_int == SIGSTOP) {
        printf("SIGSTOP\n");
    } else if (signal_int == SIGKILL) {
        printf("SIGKILL\n");
    }
}

void check_result(int result, int code) {
    if (result)
        printf("Not succeed to catch signal with code %d\n", code);
    else
        printf("Succeed to catch signal with code %d\n", code);
}

int main(int argc, char **argv) {
    int result;
    result = signal(SIGUSR1, signal_handler);
    check_result(result, SIGUSR1);

    result = signal(SIGKILL, signal_handler);
    check_result(result, SIGKILL);

    result = signal(SIGSTOP, signal_handler);
    check_result(result, SIGSTOP);

    while (1) {
        sleep(1);
        printf(".\n");
    }
}