//
// Created by Тимур Мустафин on 9/26/18.
//

#include <stdio.h>
#include <zconf.h>

int main(int argc, char **argv) {
    char first[] = "something";
    char second[100];
    int pipe_data[2];

    pipe(pipe_data);

    int process_id = fork();
    if (process_id > 0) {
        close(pipe_data[0]); // close reading stream in parent

        write(pipe_data[1], first, (sizeof(first) + 1));
        close(pipe_data[1]);
    } else if (!process_id) {
        close(pipe_data[1]); // close writing stream in child

        read(pipe_data[0], second, sizeof(second));
        printf("From pipe: %s\n", second);
        close(pipe_data[0]);
    }

}