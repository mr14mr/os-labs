//
// Created by Тимур Мустафин on 9/26/18.
//

#include <stdio.h>
#include<signal.h>

void signal_handler(int signal_int) {
    if (signal_int == SIGINT) {
        printf("CTRL + C is pressed\n");
    }
}

int main(int argc, char **argv) {
    signal(SIGINT, signal_handler);

    while (1) {
        sleep(1);
        printf(".\n");
    }
}