//
// Created by Тимур Мустафин on 11/1/18.
//

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main() {
    int first_fd = open("ex1.txt", O_RDWR);
    int second_fd = open("ex1.memcpy.txt", O_RDWR);

    struct stat first_file_stat = {};
    fstat(first_fd, &first_file_stat);

    struct stat second_file_stat = {};
    fstat(second_fd, &second_file_stat);

    void *first_ad = mmap(NULL, first_file_stat.st_size, PROT_READ | PROT_WRITE, MAP_SHARED,
                    first_fd, 0);

    void *second_ad = mmap(NULL, second_file_stat.st_size, PROT_READ | PROT_WRITE, MAP_SHARED,
                          second_fd, 0);

    unsigned int new_size = strlen(first_ad) * sizeof(char);

    ftruncate(second_fd, new_size);
    memcpy(second_ad, first_ad, new_size);

    msync(second_fd, new_size, MS_SYNC);

}