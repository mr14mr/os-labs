//
// Created by Тимур Мустафин on 11/1/18.
//

#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(){

    char str[] = "Hello";

    for(int i = 0; i < strlen(str); i++){
        printf("%c", str[i]);
        sleep(1);
    }
    printf("\n");
}