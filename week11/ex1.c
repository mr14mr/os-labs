//
// Created by Тимур Мустафин on 11/1/18.
//

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main() {
    int myfd = open("ex1.txt", O_RDWR);
    struct stat mystat = {};
    fstat(myfd, &mystat);
    void *ad = mmap(NULL, mystat.st_size, PROT_READ | PROT_WRITE, MAP_SHARED,
                    myfd, 0);

    char str [] = "This is a nice day";
    strcpy(ad, str);

    unsigned int new_size = strlen(str) * sizeof(char);

    ftruncate(myfd, new_size);

    msync(ad, new_size, MS_SYNC);

}