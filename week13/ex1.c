// Finally it's not working.

#include <stdio.h>

#define MAX_PROCESS_COUNT 10000
#define MAX_RESOURCES_COUNT 10000

int request_matrix[MAX_PROCESS_COUNT][MAX_RESOURCES_COUNT];

int alloc_matrix[MAX_PROCESS_COUNT][MAX_RESOURCES_COUNT];
int is_finished[MAX_PROCESS_COUNT];

int number_of_processes = 0;
int number_of_resources = 0;

int E[MAX_RESOURCES_COUNT];
int A[MAX_RESOURCES_COUNT];

int compare_vectors(int a[], int b[]) {
    //Is a >= b?, where >= means a[i] >= b[i] i from 0 to size
    int is_bigger = 1;

    for (int i = 0; i < number_of_resources; ++i) {
        if (a[i] > b[i])
            is_bigger = is_bigger & 1;
        else if (a[i] == b[i]) {
            is_bigger = is_bigger & 1;
        } else {
            is_bigger = 0;
            break;
        }
    }

    return is_bigger;
}

int find_the_cheapest() {
    int min_index = -1;

    for (int i = 0; i < number_of_processes; i++) {
        int is_old_bigger = 1;
        if (i != 0 && min_index != -1)
            is_old_bigger = compare_vectors(request_matrix[min_index], request_matrix[i]);


        if (is_old_bigger && !is_finished[i]) {
            min_index = i;
        }
    }

    return min_index;
}

void summarise_vectors(int a[], int b[]) {
    for (int i = 0; i < number_of_resources; ++i)
        a[i] = a[i] + b[i];

}

int get_int(char c) {
    return (int) c - 48;
}

int main() {

    int c;
    int line_number = 0;
    int number_in_line = 0;

    FILE *file;
    file = fopen("input.txt", "r");
    if (file) {

        int lines = 0;

        for (c = getc(file); c != EOF; c = getc(file))
            if (c == '\n')
                lines++;

        number_of_processes = (lines - 2) / 2; // 2 is for A and E

        file = fopen("input.txt", "r");

//        printf("Lines number %d\n", lines);

        while ((c = getc(file)) != EOF) {
            if (c == ' ') continue;
            number_in_line++;

            if (c == '\n') {
                line_number++;
                number_in_line = 0;
//                printf("\n****\n");
            } else {
                if (line_number == 0) {
                    E[number_in_line] = get_int(c);
                } else if (line_number == 1) {
                    A[number_in_line] = get_int(c);
                } else if (line_number >= 2 && line_number <= number_of_processes) {
                    alloc_matrix[line_number - 2][number_in_line] = get_int(c);
                } else {
                    request_matrix[line_number - number_in_line - 1][number_in_line] = get_int(c);
                }
//                printf("%c ", c);
            }
        }

        fclose(file);
    }

    int is_deadlock = 0;
    int processes_proceed = 0;

    while (!is_deadlock) {
        int min_index = find_the_cheapest();

        printf("New index %d\n", min_index);

        // if nothing is found just break
        if (min_index == -1)
            break;

        int is_new_fits = compare_vectors(A, request_matrix[min_index]);

        if (is_new_fits) {
            printf("No deadlock for now\n");
            is_finished[min_index] = 1;
            summarise_vectors(A, alloc_matrix[min_index]);
            processes_proceed++;
        } else {
            is_deadlock = 1;
        }
    }

    if (processes_proceed == number_of_processes && !is_deadlock) {
        printf("No deadlocks \n");

    } else {
        printf("Deadlock\n");
    }

    printf("Ended\n");
}